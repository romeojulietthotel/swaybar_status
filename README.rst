This is swaybar_status version 1.0
==================================

.. contents::

General Information
-------------------

This program writes the time and date and amount of free memory and also received/transmitted megabytes on all network interfaces on to the Swaybar. Sway is a Wayland compositor and Swaybar is the status bar for Sway. This program customizes the data that appears on the Swaybar. When I have it running I see that the RSS is about 4KB and the VSZ is around 1MB. You'd have to wonder if the numbers you see differ from these by much.
Swaybar does a "sh -c" and then runs you swaybar_status command. I have not come up with a good reason why swaybar is more complicated than it needs to be. Maybe there is some reason and I just can't see it. It should just read from stdin and that's all. No need for anything more.


Instructions
------------

Compile it:
^^^^^^^^^^^

.. code-block:: bash
		
		me@pulsar > gcc -v
		gcc version 9.1.0 (GCC)


.. code-block:: bash

		me@pulsar > gcc -Wall -o swaybar_status -static -ffunction-sections -fdata-sections -Wl,--gc-sections -fno-asynchronous-unwind-tables -fno-unroll-loops -fmerge-all-constants -fno-ident -Wl,-z,norelro -Wl,--hash-style=gnu swaybar_status.c						  


I added a CMakeLists.txt so you can also run cmake.

.. code-block:: bash

		me@pulsar > cmake .


Strip it:
^^^^^^^^^

.. code-block:: bash
		
		me@pulsar > strip -Sxs --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id --remove-section=.note.ABI-tag swaybar_status


Test it:
^^^^^^^^
This should print out lines that look like these'ns  below:

.. code-block:: bash
		
		./swaybar_status 1		
		rxMB 5983.493 | txMB 985.137 | MemFree 8137MB | Wed Aug 21 20:31:59 2119
		rxMB 5983.493 | txMB 985.137 | MemFree 8137MB | Wed Aug 21 20:32:00 2119
		rxMB 5983.493 | txMB 985.137 | MemFree 8137MB | Wed Aug 21 20:32:01 2119


Run it:
^^^^^^^

In your Sway config file at ~/.config/sway/config, in the bar {} section have this:

.. code-block:: C
		
		status_command /home/pk_dick/bin/swaybar_status 1

Then do this to reload Sway and check your Swaybar:

.. code-block:: C
		
		$mod+Shift+C

That is the key combination to reload the Sway config.


Memory consumption
------------------

.. image:: swaybar_status.jpg

	   The memory consumption is very stable and this is important as
	   the process is long-lived. Swaybar itself seems to leak out some
	   memory. It's not a lot but a leak's a leak.


TODO
----

* Add the strip command to CMakeLists.txt
* Add an optional UPX step to make the file on disk small


Links
-----

- Website: https://gitlab.com/romeojulietthotel/swaybar_status
- Source code: https://gitlab.com/romeojulietthotel/swaybar_status/blob/master/swaybar_status.c
- Issue tracker: https://gitlab.com/romeojulietthotel/swaybar_status/issues
- Documentation: https://gitlab.com/romeojulietthotel/swaybar_status/blob/master/README.rst
- Developer's Guide: https://gitlab.com/romeojulietthotel/swaybar_status/blob/master/README.rst
- Sway: https://gitlab.com/romeojulietthotel/sway
- Beyond Lies The Wub: https://www.gutenberg.org/ebooks/28554.txt.utf-8


Copyright © 2019-2029 RJH.  All rights reversed.
