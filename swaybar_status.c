/*
 *
 * Print, to stdout, some useful data. Very light on resources too.
 * I see about 4kB for RSS and ~1000kB for VSZ.
 * Copyright Â© 2019-2029 RJH.  All rights reversed
 */

#define _GNU_SOURCE

#pragma pack(1)

/* -*- mode: c -*- */

#include <ctype.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>


static void usage(const char *prog)
{
        fprintf(stderr, "Usage: %s <update interval>\n", prog);
	fprintf(stderr, "<update interval> must be an integer number of seconds\n");
	fprintf(stderr, "e.g. %s 1\n", prog);
	fprintf(stderr, "Will output every 1 second\n");
	fprintf(stderr, "Maximum sleep time is 3600 seconds.\n");
	exit( EXIT_FAILURE );
}

const char *netdevs = "/sys/class/net";

void find_rtxbytes(char *rtx)
{
	char fullthing[256] = {};
	char buf[80] = {};
	int fullsz = 0;
	unsigned long rtxbytes = 0;
	DIR *dr;
	FILE *rtxin = NULL;
	struct dirent *de;

	dr = opendir(netdevs);
	if (dr == NULL) {
		printf("Could not open: %s", netdevs);
		exit( EXIT_FAILURE );
	}
	while ((de = readdir(dr)) != NULL) {
		if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
			continue;
		fullsz = 0;
		fullsz = strlen(netdevs) +
			strlen(de->d_name) +
			strlen("statistics/") +
			strlen(rtx) +
			strlen("_bytes");
		snprintf(fullthing,
			 fullsz+3,
			 "%s/%s/%s%s%s",
			 netdevs,
			 de->d_name,
			 "statistics/",
			 rtx,
			 "_bytes");
		rtxin = fopen(fullthing, "r");
		if (!rtxin)
			continue;
		fgets(buf, sizeof(buf), rtxin);
		fclose(rtxin);
		rtxbytes += atol(buf);
	}
	closedir(dr);
	printf("| %sMB %6.3f ", rtx, rtxbytes/(1024*1024*1.000));
	rtxbytes = 0;
}

int mem_free(void)
{
	unsigned long mf = 0;
	char *line = 0;
	size_t linelen = 0;
	FILE *f = fopen("/proc/meminfo", "r");

	if (!f)
		return 0;
	while (getline(&line, &linelen, f) > 0) {
		/* skip invalid lines */
		if (!strchr(line, ':'))
			continue;
		if (sscanf(line, "MemFree:       %lu kB", &mf) == 1) {
			mf >>= 10;
			printf("| MemFree %ldMB ", mf);
			fclose(f);
			free(line);
			return 0;
		}
	}
	free(line);
	return 0;
}

int main(int argc, char *argv[])
{
	char *argv1 = 0;
	int i = 0, interval = 0, sz = 0;
	time_t t = time(NULL);
	char myout[80] = {};
	
	/*
	 * man 3 asctime_r.
	 * ...
	 * but stores the string in a user-supplied
	 * buffer which should have room for at least 26 bytes.
	 *
	 */
	char mydate[26] = {};
	struct sigaction act;
	act.sa_handler = SIG_IGN;

	
	if ( argc != 2 ) {
	        usage(argv[0]);
	}
	if (strlen(argv[1]) < 1 || strlen(argv[1]) > 4) {
	        usage(argv[0]);
	}
	argv1 = argv[1];
	sz = strlen(argv1);
	for (i = 0; i < sz; i++) {
		if (!isdigit(argv1[i]))
			usage(argv[0]);
	}
	interval = atoi(argv1);
	if (interval < 1)
		usage(argv[0]);
	if (interval > 3600)
		interval = 3600;

	/* 
	 * swaybar sends SIGCONT(18) or SIGSTOP(19)
	 * so we will let it do that.
	 * Signals 32/33 are not defined in signal(7).
	 */ 
	for(int i = 1 ; i < 65 ; i++) {
		if((i != 18) && (i != 19) && (i != 32) && (i != 33)) {
			sigaction(i, &act, NULL);
		}
	}
	while(1) {
		t = time(NULL);
		asctime_r(localtime(&t), mydate);
		t = 0;
		find_rtxbytes("rx");
		find_rtxbytes("tx");
		mem_free();
		printf("| %s", mydate);
		fputs(myout,stdout);
		fflush(stdout);
		*myout = '\0';
		sleep(interval);
	}
	exit( EXIT_SUCCESS );
}
